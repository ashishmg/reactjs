var Note = React.createClass({
  getInitialState: function(){
    return {editing: false}
  },
  edit: function(){
    this.setState({editing: true})
  },
  save: function(){
    var val = this.refs.newText.getDOMNode().value;
    alert("todo save note value" + val);
    this.setState({editing: false})
  },
  remove: function(){
    alert("removeing note");
  },
  renderDisplay: function(){
    return (
        <div className="note">
        <p>{this.props.children}</p>
        <span>
          <button className="btn btn-primary glyphicon glyphicon-pencil" onClick={this.edit}/>
          <button className="btn btn-danger glyphicon glyphicon-trash" onClick={this.remove} />
        </span>
        </div>
        );
  },
  renderForm: function(){
    return(
      <div className="note">
        <textarea ref="newText" defaultValue={this.props.children} className="form-control"></textarea>
        <button className="btn btn-success btn" onClick={this.save} />

      </div>
    )
  },

    render: function() {
      if(this.state.editing){
        return this.renderForm();
      }
      else {
        return this.renderDisplay();
      }
    }
});
var Board = React.createClass({
  propTypes: {
      count: function(props, propertyName){
        if(typeof props[propName] !== "number")
{
  return new Error("the count property must ba number");
}
if (props[propName] > 100){
  return new Error("Creating" + props[propName] + "notes is redeculasu");

}
      }
  },
  getInitialState: function(){
    return{
      notes: [
        'ashish','mangla','love'
      ]
    };
  },
  render: function(){
    return (
      <div className="board">
        {this.state.notes.map(function(note, i){
          return(
            <Note key={i}>{note}</Note>
          );
        })}
      </div>
    )

  }
})

React.render(<Board count= {10}/>,
    document.getElementById('react-container'));
